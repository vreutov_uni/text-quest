import socketIoClient from "socket.io-client";
import sailsIo from "sails.io.js";

let io = sailsIo(socketIoClient);
let url = "http://localhost:1337";

io.sails.url = url;
io.sails.environment = "development";
io.sails.useCORSRouteToGetCookie = false;

io.socket.on("disconnect", function() {
  io.socket._raw.io._reconnection = true;
});

export default {
  data: function() {
    return {
      get io() {
        return io;
      }
    };
  }
};
