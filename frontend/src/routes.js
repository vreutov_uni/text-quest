import AppHeader from "./components/AppHeader";
import GameList from "./components/GameList";
import GameEdit from "./components/GameEdit";
import GamePlay from "./components/GamePlay";
import NotFound from "./components/404";

export default [
  {
    path: "/404",
    components: {
      default: NotFound,
      header: AppHeader
    },
    props: { header: { caption: "Дорогу осилит идущий" } }
  },
  {
    path: "/",
    meta: {
      title: "TextQuest - система управления текстовыми квестами"
    },
    name: "home",
    components: {
      default: GameList,
      header: AppHeader
    },
    props: { header: { caption: "Список игр" } }
  },
  {
    path: "/edit/:id",
    name: "edit-game",
    meta: {
      title: "TextQuest - редактирование игры"
    },
    components: {
      default: GameEdit,
      header: AppHeader
    },
    props: { header: { caption: "Редактирование" } }
  },
  {
    path: "/play/:id",
    name: "play-game",
    meta: {
      title: "TextQuest - режим игры"
    },
    components: {
      default: GamePlay,
      header: AppHeader
    },
    props: { header: { caption: "Игра, еее" } }
  }
];
