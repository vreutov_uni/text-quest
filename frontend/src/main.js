import Vue from "vue";
import VueRouter from "vue-router";
import iomixin from "./sails-io-client";

import Element from "element-ui";
import locale from "element-ui/lib/locale/lang/ru-RU";
import "normalize.css";
import "./element-variables.scss";

import App from "./App.vue";
import routes from "./routes";

Vue.use(VueRouter);
Vue.use(Element, { locale });

Vue.mixin(iomixin);

Vue.config.productionTip = false;

const router = new VueRouter({
  mode: "history",
  routes
});

router.beforeEach((to, from, next) => {
  document.title = to.meta.title;
  next();
});

new Vue({
  router,
  render: h => h(App)
}).$mount("#app");
