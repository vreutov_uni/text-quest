module.exports = {
  friendlyName: 'Get frame',

  description: 'Get frame by its name',

  inputs: {
    gameId: {
      description: 'The ID of the game to play',
      type: 'number',
      required: true
    },
    name: {
      description: 'The name of the frame to find',
      type: 'string',
      required: true
    },
  },

  exits: {
    success: {
    },
    gameNotFound: {
      description: 'No game with the specified ID was found in the database.',
      responseType: 'notFound'
    },
    frameNotFound: {
      description: 'No frame with the specified name was found in the database.',
      responseType: 'notFound'
    },
  },

  fn: async function (inputs, exits) {
    let game = await Game.findOne(inputs.gameId).populate('knowledgeBase');
    if (!game) { return exits.gameNotFound(); }

    let frame = _.find(game.knowledgeBase.frameInstances, (frame) =>
      frame.name === inputs.name
    );

    if (!frame) { return exits.frameNotFound(); }

    return exits.success(frame);
  }
};
