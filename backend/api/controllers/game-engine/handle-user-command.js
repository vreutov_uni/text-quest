function getFrame(game, name) {
  let frames = game.knowledgeBase.frameInstances;

  let result = _.find(frames, (frame) =>
    frame.name === name
  );

  return (result !== undefined ? result : null);
}

function clearCommand(command) {
  return command.toLowerCase().trim();
}

function commandCallsToAction(game, command, action) {
  return _.some(action.slots.commands.value, (knownCommand) => {
    if (clearCommand(knownCommand) === command) {
      return true;
    }

    let object = getFrame(game, action.slots.object.value);
    complexCommand = clearCommand(knownCommand + ' ' + object.slots.name.value);

    if (complexCommand === command) {
      return true;
    }

    return false;
  });
}

function performAction(game, session, action, context) {
  if (_.includes(session.data.doneActions, action.name)) {
    return false;
  }

  let premise = action.slots.premise.value;
  if (premise === null || _.includes(session.data.doneActions, premise)) {
    let hasTools = !_.some(action.slots.tools.value,
      (tool) => !_.includes(session.inventory, tool));

    if (hasTools) {
      session.data.inventory.push(...action.slots.reward.value);
      session.data.inventory = _.filter(session.data.inventory,
        (item) => !_.includes(action.slots.loss.value, item));

      session.data.doneActions.push(action.name);
      if (action.slots.object.value !== null) {
        session.data.usedItems.push(action.slots.object.value);
      }

      for (let transit of context.transits) {
        if (transit.slots.premise.value === action.name && transit.slots.auto.value) {
          return performTransit(game, session, transit);
        }
      }

      return true;
    }
  }

  return false;
}

function commandCallsToTransit(game, command, action) {
  return _.some(action.slots.commands.value, (knownCommand) => {
    return clearCommand(knownCommand) === command;
  });
}

function performTransit(game, session, transit) {
  let premise = transit.slots.premise.value;
  if (premise === null || _.includes(session.data.doneActions, premise)) {
    session.data.currentLocation = transit.slots.direction.value;
    session.data.doneActions.push(transit.name);
    return true;
  } else {
    return false;
  }
}

module.exports = {
  friendlyName: 'Handle user command',

  description: '',

  inputs: {
    gameId: {
      description: 'The ID of the game to play',
      type: 'number',
      required: true
    },
    session: {
      description: 'The GameSession object',
      type: 'json',
      required: true
    },
    command: {
      description: 'The command given by user',
      type: 'string',
      required: true
    },
  },

  exits: {
    success: {
    },
    gameNotFound: {
      description: 'No game with the specified ID was found in the database.',
      responseType: 'notFound'
    },
    unknownCommand: {
      statusCode: 400 // Bad Request
    },
    cantPerformCommand: {
      statusCode: 405 // Method Not Allowed
    }
  },

  fn: async function (inputs, exits) {
    let game = await Game.findOne(inputs.gameId).populate('knowledgeBase');
    if (!game) { return exits.notFound(); }

    let session = inputs.session;
    let location = getFrame(game, session.data.currentLocation);

    let command = clearCommand(inputs.command);

    let questActions = _.map(location.slots.actions.value, (a) => getFrame(game, a));
    let transits = _.map(location.slots.transits.value, (t) => getFrame(game, t));

    let context = {
      location,
      transits,
      questActions
    };

    async function tryDoAction(action, predicate, perform) {
      if (predicate(game, command, action, context)) {
        if (perform(game, session, action, context) === true) {
          session = await GameSession.updateOne(session.id).set({
            data: session.data
          });

          return () => exits.success({session, message: action.slots.success.value});
        } else {
          return () => exits.cantPerformCommand({session, message: action.slots.fail.value});
        }
      }
    }

    for (let questAction of questActions) {
      let callback = await tryDoAction(questAction, commandCallsToAction, performAction);
      if (callback !== undefined) {
        return callback();
      }
    }

    for (let transit of transits) {
      let callback = await tryDoAction(transit, commandCallsToTransit, performTransit);
      if (callback !== undefined) {
        return callback();
      }
    }

    return exits.unknownCommand();
  }
};
