var _ = require('lodash');

function getStartLocation(game) {
  let frames = game.knowledgeBase.frameInstances;
  let startLocationType = game.knowledgeBase.types.locationType.values[0];
  let startLocation = _.find(frames, (frame) =>
    frame.parentFrame === 'location' && frame.slots.type.value === startLocationType
  );

  return (startLocation !== undefined ? startLocation : null);
}

module.exports = {
  friendlyName: 'Start new game',

  description: '',

  inputs: {
    gameId: {
      description: 'The ID of the game to play',
      type: 'number',
      required: true
    }
  },

  exits: {
    success: {
    },
    notFound: {
      description: 'No game with the specified ID was found in the database.',
      responseType: 'notFound'
    },
    noStartLocation: {
      description: 'Game\'s knowledge base has no start location.',
    }
  },

  fn: async function (inputs, exits) {
    let game = await Game.findOne(inputs.gameId).populate('knowledgeBase');

    if (!game) { return exits.notFound(); }

    if (game.session !== null) {
      await GameSession.destroyOne(game.session);
    }

    let session = await GameSession.create({
      game: game.id,
      gameVersion: game.knowledgeBase.version
    }).fetch();

    await Game.updateOne(game.id).set({
      session: session.id
    });

    let startLocation = getStartLocation(game);

    if (startLocation !== null) {
      let data = session.data;

      data.currentLocation = startLocation.name;
      session = await GameSession.updateOne(session.id).set({
        data: data
      });

      return exits.success(session);
    } else {
      await GameSession.destroyOne(session.id);
      return exits.noStartLocation();
    }
  }
};
