module.exports = {
  friendlyName: 'Load expanded session',

  description: 'Get session with expanded location and inventory items',

  inputs: {
    gameId: {
      description: 'The ID of the game to play',
      type: 'number',
      required: true
    },
    sessionId: {
      description: 'The ID of the game session',
      type: 'string',
      required: true
    },
  },

  exits: {
    success: {
    },
    gameNotFound: {
      description: 'No game with the specified ID was found in the database.',
      responseType: 'notFound'
    },
    sessionNotFound: {
      description: 'No session with the specified ID was found in the database.',
      responseType: 'notFound'
    },
    sessionDoNotMatchGame: {
      description: 'Given session doesnt match game with the specified ID.',
      statusCode: 400 // Bad Request
    }
  },

  fn: async function (inputs, exits) {
    let game = await Game.findOne(inputs.gameId).populate('knowledgeBase');
    if (!game) { return exits.notFound(); }

    let session = await GameSession.findOne(inputs.sessionId);
    if (!session) { return exits.notFound(); }

    if (session.game !== game.id) { return exits.sessionDoNotMatchGame(); }

    function getFrame(name) {
      let result = _.find(game.knowledgeBase.frameInstances, (frame) =>
        frame.name === name
      );
      return (result !== undefined ? result : null);
    }

    session.data.currentLocation = getFrame(session.data.currentLocation);
    session.data.inventory = _.map(session.data.inventory, getFrame);

    items = session.data.currentLocation.slots.items.value;
    session.data.currentLocation.slots.items.value = _.map(items, getFrame);

    return exits.success(session);
  }
};
