module.exports = {
  attributes: {
    name: {
      type: 'string',
      required: true
    },
    description: {
      type: 'string'
    },
    session: {
      model: 'gamesession'
    },
    knowledgeBase: {
      model:'knowledgebase'
    }
  },

  beforeCreate: async function(newGame, proceed) {
    let knowledgeBase = await KnowledgeBase.create().fetch();
    newGame.knowledgeBase = knowledgeBase.id;

    return proceed();
  },

  afterCreate: async function(newGame, proceed) {
    await KnowledgeBase.update({
      id: newGame.knowledgeBase.id
    }).set({
      game: newGame.id
    });

    return proceed();
  }
};
