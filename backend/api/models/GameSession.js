module.exports = {
  attributes: {
    game: {
      model: 'game'
    },
    gameVersion: {
      type: 'string',
      defaultsTo: '0.0.0'
    },
    data: {
      type: 'json'
    }
  },

  beforeCreate: function(newInst, proceed) {
    newInst.data = {
      currentLocation: null,
      inventory: [],
      doneActions: [],
      usedItems: []
    };

    return proceed();
  }
};
