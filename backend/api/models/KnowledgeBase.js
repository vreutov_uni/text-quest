/* eslint quotes: ["warn", "double"] */

module.exports = {
  attributes: {
    game: {
      model: "game"
    },
    version: {
      type: "string",
      defaultsTo: "0.0.0"
    },
    types: {
      type: "json"
    },
    baseFrames: {
      type: "json"
    },
    frameInstances: {
      type: "json"
    }
  },

  beforeCreate: function(newInst, procced) {
    newInst.types = {
      baseTypes: {
        values: ["string", "int", "bool", "list", "readOnly"]
      },
      locationType: {
        values: [
          "Стартовая",
          "Промежуточная",
          "Конечная (победа)",
          "Конечная (поражение)"
        ]
      }
    };
    newInst.baseFrames = [
      {
        name: "entity",
        description: "сущность",
        parentFrame: null,
        slots: {
          name: {
            label: "Название",
            type: "string"
          }
        }
      },
      {
        name: "object",
        description: "объект",
        parentFrame: "entity",
        slots: {
          descr: {
            label: "Описание",
            type: "string"
          }
        }
      },
      {
        name: "item",
        description: "предмет",
        parentFrame: "object",
        slots: {
          canTake: {
            label: "Можно взять",
            type: "bool"
          },
          descrInLocation: {
            label: "Описание в интерьере",
            type: "string"
          },
          descrАfterUse: {
            label: "Описание после использования",
            type: "string"
          }
        }
      },
      {
        name: "interior-item",
        description: "предмет окружения",
        parentFrame: "item",
        slots: {
          canTake: {
            constValue: false
          }
        }
      },
      {
        name: "quest-item",
        description: "квестовый предмет",
        parentFrame: "item",
        slots: {
          canTake: {
            constValue: true
          },
          descrАfterUse: {
            constValue: null
          },
          failMessage: {
            label: "Отказ",
            type: "string",
            default: "Не могу"
          }
        }
      },
      {
        name: "action",
        description: "действие",
        parentFrame: "entity",
        slots: {
          commands: {
            label: "Команды",
            type: "list",
            listItemType: "string"
          },
          premise: {
            label: "Предпосылка",
            type: "action"
          },
          success: {
            label: "Успех",
            details: "Сообщение пользователю, если действие выполнено успешно",
            type: "string"
          },
          fail: {
            label: "Неудача",
            details:
              "Сообщение пользователю, если не удалось выполнить действие",
            type: "string"
          }
        }
      },
      {
        name: "transit",
        description: "действие перехода",
        parentFrame: "action",
        slots: {
          commands: {
            default: ["перейти на"]
          },
          direction: {
            label: "Направление",
            type: "location"
          },
          auto: {
            label: "Автопереход",
            type: "bool",
            default: false
          },
          reverse: {
            label: "Обратимый",
            type: "bool",
            default: true
          }
        }
      },
      {
        name: "quest-action",
        description: "квестовое действие",
        parentFrame: "action",
        slots: {
          commands: {
            default: ["использовать"]
          },
          object: {
            label: "Объект",
            type: "item",
            default: []
          },
          tools: {
            label: "Средства",
            type: "list",
            listItemType: "quest-item",
            default: []
          },
          reward: {
            label: "Награда",
            details:
              "Список предметов, которые получит игрок при выпонении действия",
            type: "list",
            listItemType: "quest-item",
            default: []
          },
          loss: {
            label: "Потери",
            details:
              "Список предметов, которые игрок потеряет при выпонении действия",
            type: "list",
            listItemType: "quest-item",
            default: []
          }
        }
      },
      {
        name: "location",
        description: "локация",
        parentFrame: "object",
        slots: {
          type: {
            label: "Тип локации",
            type: "locationType"
          },
          items: {
            label: "Интерьер",
            type: "list",
            listItemType: "item",
            default: []
          },
          transits: {
            label: "Переходы",
            type: "list",
            listItemType: "transit",
            default: []
          },
          actions: {
            label: "Действия",
            type: "list",
            listItemType: "quest-action",
            default: []
          }
        }
      }
    ];

    newInst.frameInstances = [];

    return procced();
  }
};
