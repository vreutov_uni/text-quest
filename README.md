# text-quest

## Требования
* [Node.js](https://nodejs.org/en/download/) >= 8.11.3
* (опционально) [Sails.js](https://sailsjs.com/get-started) >= 1.0.2

## Установка
```
cd frontend && npm install
cd ../backend && npm install
```
## Запуск
### Backend

Если установлен Sails.js:
```
cd backend && sails lift
```
Иначе:
```
cd backend && node app.js
```

### Frontend
```
cd frontend && npm run serve
```